# Documentation
The entire architecture is based on a dataset on Kaggle:
[Dataset](https://www.kaggle.com/therohk/million-headlines) (License: CC0: Public Domain)

It contains a CSV file with one million news headlines together with a publishing date. On the basis of this data we create several evaluations of this data. Because we have no realtime data source, such as a news API, we pretend to be at a point in the past, where the headlines where actually published. In practice this means that we have a producer that emits the headlines at a limited rate instead of putting out the entire dataset at once and thus having a streaming data source without depending on a real API.

Some of the evaluations we do are:
* Latest published articles, which we use as the server time / the pretended point in time were we currently are.
* Total amount of published articles (at the pretended time).
* News articles published per day.
* Word counts in the headlines without some common words such as "the".
* Preprocessed top lists of the other evaluations to avoid doing huge time consuming *order by* and *limit* operations on the SQL database. For example for the top 100 most viewed articles.

The evaluations are processed with a Kappa architecture, in a microbatching manner.

Another data source are views on articles by users in the web frontend. This is done by having the backend emit a message on Kafka everytime the frontend requests a single article over the according REST endpoint. This message is then handled by our Spark application to provide for example the following evaluations:
* Views per Article
* Total views on all articles

A screencast is in [doc/screencast.mp4](doc/screencast.mp4).

## Architecture
<img src="images/architecture.png" width=100%>

### Ingress
Due to the ingress, frontend and backend are reachable on the same IP-Address. The path '/api' leads to the backend. Every other path is handled in the frontend. The default frontend route '/' leads to the home component, which is the dashboard. With this deployment, the IP-Address is not needed for requests of data from the rest-api. It is sufficient to request data from the path '/api' because in that case, the same IP-Address as the frontend will be used.

The frontend is deployed on a nginx webserver. This nginx needs an explicit configuration to allow routing in frontend. With this configuration, all requested routes are forwarded to the index.html of frontend-app, so the frontend can perform the routing.

## Microservices

### News Producer
The **News Producer** reads line for line from CSV **abcnews-date-text_full.csv**, which contains about 1 million newslines. Afterwards the data is send to the Kafka topic **headlines** with some delay between messages.

### Spark
The Spark Application reads from two 2 Kafka-Topics **headlines** and **article_view** and processes the data in microbatches.
#### Topic: headlines
Firstly the data from Topic **headlines**, which is produced by the News Producer, is converted from an CSV-style into publish_date and headline_text (as String). Afterwards publish_date gets converted into date_type.

The result is a Dataframe, which is used in 4 Pipelines:

1. Number of articles per date <br/> --> saved into table **day_count**
2. All articles and dates <br/> --> saved into table **article**
3. Split the headline into words, count the apperance of each word and filter common english words <br/> --> saved into table **word_count**
4. Get the latest date of the current batch <br/> --> saved into table **server_time**

#### Topic: article_view
The data in topic **article_view** is produced by the Web-Application itself. The data format is json and consists of an (article-)id and a timestamp. After a conversion into a dataframe, the data will be used in 2 pipelines:

1. Count the Views of an article <br/> --> saved into table **article_views**
2. Last Access of an article <br/> --> saved into table **article_views**

#### Further Savings
There are some more savings into different tables:

1. In the article batch job, the current total count of articles in the table **articles** is saved into the table **sum_article_count** . The table is truncated before each saving.
2. In the article_view batch job, the top 100 articles based on the view count is saved into a seperate table **top_articles**. Furthermore the total count of views is calculated and saved into the table **sum_view_count**.
3. In the word_count batch job, the top 100 words, based on the appearance of each word, is saved into a seperate table **top_words**. 
The table is truncated before each saving as well.

All tables are stored on (Postgres-)Database **news**.

#### Update notifications
After each batch job, the Spark Application sends a message to Kafka topic **batch_complete**, which contains the name of the completed batch. The produced data will be processed in the Message Bridge service.

### Backend
The backend was developed in Python and uses most two libraries/frameworks: Flask and Flask-RestX. Flask is used as the foundation and handles http-requests as a web server. Flask-RestX allows us to declaratively create REST endpoints on this foundation and automatically creates a Swagger/OpenAPI specification for these endpoints (a snapshot of a generated specification is in [doc/swagger.json](doc/swagger.json)). The library Flask-Caching extends the declarative definitions by providing function decorators to cache results of endpoints (See [Caching](caching.md) for more information). Access to the PostgreSQL database is done over the ORM library Flask-SQLAlchemy, which allows the declarative definition of database objects/tables. These declarative definitions are shared between the microservices to allow an abstract access to entities in the database without having to update each service seperately, when the database scheme changes.

Furthermore the backend acts as a Socket.io worker. By using sticky sessions in the Ingress service, the same backend server serves the same client everytime and can therefore keep a websocket connection alive or react to polling requests. When a message is emitted the according backend server with the connection to the target needs to handle this request or when a message is broadcasted, it needs to be forwarded by every worker. This coordination is done over a message queue via Kafka. This queues also allows services to send messages to clients without having a direct connection to them, as is the case in the Message Bridge.

### Message Bridge
The message bridge forwards some messages from Kafka to Socket.io. This is done by subscribing to certain topics on Kafka and putting them on the Socket.io message queue to have them emitted to clients by the workers in the backend. We do this for information hiding, so that other services can be kept unaware of a any connection to browser clients. The News Producer for example doesn't have and shouldn't now that there are other consumers than on the Kafka queue that handle it's requests.

Information forwarded by the Message Bridge are:
* Headlines: So we can debug the incoming data in the frontend.
* Batch Completions: Whenever spark completes a bactch, it puts a notification on the *batch_complete* topic. This notification is forwarded to the webclients to update for example the dashboard automatically without having to refresh the page, whenever the Spark processing provides new data.

### Frontend
The frontend was developed in angular using typescript. For styling the angular material-design was used, so the dashboard could have separate cards for the different evaluations. Angular apps are constructed from multiple components which are combined in the html code. So, every presentation of data had an own component. These components were included in the card-content. These cards are included in the home component. The article view and debug page are constructed in the same manner. This structure has the advantage that every component is easily exchangeable, so adjustment of the dashboard can be done fast.

#### Service Layer
Two types of Angular services have been implemented. On the one hand HTTP services and on the other hand a socket.io service. 

An HTTP service in this context represents an internal interface, meaning only for the frontend components. It provides methods that encapsulate the API calls of the backend's REST interface. Primarily, these are GET requests, since the task of the frontend is just to visualize data in the first place, and thus only data needs to be retrieved. By grouping the API calls through this mid-layer, there is the advantage that same accesses implementations do not occur multiple times in different components. Altogether the maintainability increases, since there are no redundancies which must be considered with any API changes. A component gets a service by constructor dependency injection. These loose couple makes it possible to exchange or extend services with relatively small effort, also on a long-term basis. Overall, a service always enables access to exactly one endpoint route group, such as the route "/api/word-count" with its three endpoints.

The service, that does not belong to the HTTP service category, is the SocketIoService. This service represents a WebSocket client and connects to the WebSocket server on the backend under the endpoint "/api/socket.io". The JavaScript library [socket.io-client](https://www.npmjs.com/package/socket.io-client) is used for this purpose. This service contains only one subscribe method. Using this method, any component can subscribe to one or more of the two topics (see [Message Bridge](#message-bridge])) and thus obtains all messages on this topic via the message queue. The method returns an Observable object, which calls a defined Call-Back method when a new message occurs. Through this method, a component knows that new data is available thus it can retrieve them by performing the appropriate requests to the backend obtaining the data. Finally, a component can update and display the data in real time on the graphical user interface.