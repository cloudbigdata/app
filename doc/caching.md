# Caching
## Server
The actual storage of cached data is taken care of by a scalable deployment of Memcached instances. A Kubernetes service provides a hostname for these (separate) instances. This allows us to resolve the individual servers by the common hostname or via a DNS query.

Sources: [Memcached Website](https://www.memcached.org/)

## Client
The cache is only accessed by our backend (the api-server) which uses Flask as it's base framework. Therefore we chose a library that is already integrated with this framework and easily allows the caching of entire views or endpoints. The chosen library was Flask-Caching, which is a fork of the abandoned Flask-Cache. Flask-Caching allows the usage of several cache backends such as local storage in a python dictionary, Redis or our choice, Memcached. We extended the included backend by overloading some methods to set some http-headers such as x-cache to inspect cache hits/misses in the frontend.

Sources: [Flask-Caching](https://pythonhosted.org/Flask-Caching/)