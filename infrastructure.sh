#!/bin/bash
# Usage: ./infrastructure.sh [mode]
# Modes:
#   No mode supplied: Delete and then apply
#   "apply": Apply only
#   "delete": Delete only
if [ "$1" == "delete" ];
then 
kubectl delete -f k8s/infrastructure/kafka.yaml
kubectl delete -f k8s/infrastructure/postgres.yaml
kubectl delete -f k8s/infrastructure/memcached.yaml
fi

if [ "$1" == "apply" ];
then 
    kubectl apply -f k8s/infrastructure/kafka.yaml
    kubectl apply -f k8s/infrastructure/postgres.yaml  
    kubectl apply -f k8s/infrastructure/memcached.yaml
fi
if [ "$1" == "alles" ];
then 
	kubectl delete -f k8s/infrastructure/kafka.yaml
	kubectl delete -f k8s/infrastructure/postgres.yaml
	kubectl delete -f k8s/infrastructure/memcached.yaml
	kubectl delete pod --all 
	kubectl delete replicasets.apps --all
	kubectl delete deployments.apps --all
	kubectl delete statefulsets.apps --all
	kubectl delete service memcached-service
	kubectl delete service my-cluster-zookeeper-client 
	kubectl delete service my-cluster-zookeeper-nodes	
	kubectl delete service my-cluster-kafka-brokers
	kubectl delete service my-cluster-kafka-bootstrap
	kubectl delete service postgres-service 
fi
