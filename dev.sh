#!/bin/bash
if [[ -z "${K3S_MASTER_IP}" ]]; then
  echo "Please set the environment variable K3S_MASTER_IP to the public IP of your cluster."
  exit 1
else
  skaffold dev --default-repo $K3S_MASTER_IP:5000
fi
