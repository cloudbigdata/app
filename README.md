## News App
App to generate a dashboard for news headlines.

For further documentation see:
[Documentation](doc/README.md)

### Deployment
#### Development
Have the following applications installed:
* docker
* kubectl
* strimzi operator on Kubernetes

**The environment variable K3S_MASTER_IP must be set to the public IP / Docker registy IP of your cluster.**

Run the following commands:
```shell
./infrastructure.sh apply # Creates the required infrastructure services
./dev.sh # Starts and monitors only our applications
```
This will build the apps docker images and deploy them on the specified Kubernetes cluster.
The app files are then monitored for changes and automatically redeployed if necessary.

