from flask_restx import fields, reqparse
from app import api

pagination_parser = reqparse.RequestParser()
pagination_parser.add_argument('page', type=int, help='Page to retrieve (starts at 1)', required=False)
pagination_parser.add_argument('psize', type=int, help='Page size (default: 20, max: 1024)', required=False)

pagination_model = api.model('PaginationMetadata', {
    'page': fields.Integer,
    'pagesTotal': fields.Integer(attribute='pages'),
    'perPage': fields.Integer(attribute='per_page'),
    'hasNext': fields.Boolean(attribute='has_next'),
    'hasPrev': fields.Boolean(attribute='has_prev'),
    'resultsTotal': fields.Integer(attribute='total')
})
