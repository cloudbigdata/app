import pylibmc
from threading import Thread
from time import sleep
from app.controller.cache.memcached_discovery import get_servers
from app import app, config, cache

def repeated_discovery(hostname: str, interval: int) -> None:
    """
    Searches for memcached servers at a given interval and
    replaces the global cache instance if there was a change.

    Args:
        hostname: Hostname for (multiple) memcached server(s)
        interval: Interval between searches
    """
    servers = {}
    while True:
        new_servers = set(get_servers(hostname))
        if new_servers != servers:
            print(f"Memcached servers changed: {new_servers}")
            servers = new_servers
            if hasattr(cache.cache, '_client'): # Server type is memcached and not simple
                app.config['CACHE_MEMCACHED_SERVERS'] = list(servers)
                cache.cache._client = pylibmc.Client(list(servers), binary=True,
                                                        behaviors={"tcp_nodelay": True})
        sleep(interval)


thread = Thread(target=repeated_discovery, args=(config.get('Cache', 'hostname'), 
                                                 config.get_int('Cache', 'discovery_interval')))
thread.start()
