import datetime
import json
from functools import wraps
from werkzeug.exceptions import NotFound
from app import kafka_producer


def publish_article_view(f):
    @wraps(f)
    def inner(*args, **kwargs):
        try:
            result = f(*args, **kwargs)
            kafka_producer.send("article_views", json.dumps({ 'id': kwargs.get('id'), 'timestamp': datetime.datetime.now().isoformat() }).encode())
            return result
        except NotFound:
            raise NotFound()
    return inner
