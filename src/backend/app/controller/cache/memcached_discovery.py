import socket
from typing import List


def get_servers(hostname: str) -> List[str]:
    """
    Discovers memcached servers and returns them as a list.

    Args:
        hostname: Hostname for (multiple) memcached server(s)
    
    Returns:
        A list of memcached servers with ports matching the hostname.
    """
    try:
        servers = socket.gethostbyname_ex(hostname)[2]
        return list(map(lambda s: s + ':11211', servers))
    except Exception:
        return ['127.0.0.1:11211']
