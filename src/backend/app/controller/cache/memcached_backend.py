from functools import wraps
from flask import g
from flask_caching.backends import MemcachedCache


class CustomCache(MemcachedCache):
    """
    Cache backend that extends the integrated Memcache backend.
    It adds debugging information such as http-headers an catches
    exception that are (for some reason) thrown by the c++ library
    behind pylibmc. Theses errors occur just occassionally, therefore
    we just ignore them and fail silently, so that it justs counts as 
    a cache miss.
    """
    def has(self, key):
        result = super().has(key)
        #print(f"Cache: HAS {key} -> {'Yes' if result else 'No'}")
        return result
    
    def set(self, key, value, timeout=None):
        try:
            super().set(key, value, timeout)
        except Exception as e:
            print(f"Cache: SET {key} -> {e}")
            pass

    def get(self, key):
        try:
            result = super().get(key)
            if key.startswith('view/'):
                g.cached = result is not None
            #print(f"Cache: GET {key} -> {result if result is not None else 'miss'}")
            return result
        except Exception as e:
            print(f"Cache: GET {key} -> {e}")
            return None
    
    def get_dict(self, *keys):
        result = super().get(keys)
        #print(f"Cache: GET DICT {keys} -> {result if result is not None else 'miss'}")
        return result


def create(app, config, args, kwargs):
    args.append(config["CACHE_MEMCACHED_SERVERS"])
    kwargs.update(dict(key_prefix=config["CACHE_KEY_PREFIX"]))
    return CustomCache(*args, **kwargs)


def cache_api_with_parser(name, req_parser):
    from app import cache
    def outer(f):
        @wraps(f)
        def inner(*args, **kwargs):
            parsed_args = req_parser.parse_args()
            cache_name = "view/" + name
            for arg in parsed_args:
                cache_name += "/" + str(parsed_args[arg])
            cached = cache.get(cache_name) 
            if not cached:
                result = f(*args, parsed_args, **kwargs)
                cache.set(cache_name, result)
                return result
            return cached
        return inner
    return outer