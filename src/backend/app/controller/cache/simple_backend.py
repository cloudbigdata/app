from flask import g
from flask_caching.backends import SimpleCache

class CustomSimpleCache(SimpleCache):    
    def get(self, key):
        result = super().get(key)
        if key.startswith('view/'):
            g.cached = result is not None
        #print(f"Cache: GET {key} -> {result if result is not None else 'miss'}")
        return result


def create(app, config, args, kwargs):
    kwargs.update(
        dict(
            threshold=config["CACHE_THRESHOLD"],
            ignore_errors=config["CACHE_IGNORE_ERRORS"],
        )
    )
    return CustomSimpleCache(*args, **kwargs)