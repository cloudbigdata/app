from werkzeug.utils import find_modules, import_string


def import_threads():
    for module_name in find_modules('app.controller.threads'):
        import_string(module_name)