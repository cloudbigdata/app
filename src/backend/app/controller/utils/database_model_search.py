from time import sleep
from werkzeug.utils import find_modules, import_string


def wait_for_database(db):
    while True:
        try:
            db.engine.execute("select 1")
            print("Connected to database.")
            return
        except:
            print("Cannot connect to database. Trying again in 5s...")
            sleep(5)


def import_database_models(db):
    wait_for_database(db)
    for module_name in find_modules('shared.database'):
        import_string(module_name)
    db.create_all()