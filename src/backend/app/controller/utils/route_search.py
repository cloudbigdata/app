from werkzeug.utils import find_modules, import_string


def import_routes():
    for module_name in find_modules('app.view'):
        import_string(module_name)