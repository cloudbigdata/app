import datetime
from werkzeug.exceptions import BadRequest
from flask_restx import Namespace, Resource, fields
from app import api, cache
from app.controller.cache.memcached_backend import cache_api_with_parser
from app.model.api.pagination import pagination_parser, pagination_model
from shared.database.day_count import DayCountModel

namespace = Namespace('day-count', description='Routes to retrieve the number of articles per day')
api.add_namespace(namespace)

day_count_model = api.model('DayCount', {
    'date': fields.Date,
    'count': fields.Integer
})
day_count_paginated_model = api.model('DayCountPaginated', {
    'pagination': fields.Nested(pagination_model),
    'items': fields.Nested(day_count_model, as_list=True)
})


@namespace.route('/')
class DayCountsResource(Resource):
    @cache_api_with_parser('DayCountsPaginated', pagination_parser)
    @namespace.marshal_with(day_count_paginated_model)
    @namespace.expect(pagination_parser)
    def get(self, args):
        page = 1 if args['page'] is None else args['page']
        page_size = 20 if args['psize'] is None else min(args['psize'], 1024)
        query = DayCountModel.query.order_by(DayCountModel.date.desc())
        pagination = query.paginate(page=page, per_page=page_size, error_out=True)
        return {
            'pagination': pagination,
            'items': pagination.items
        }


@namespace.route('/<int:year>/<int:month>/<int:day>')
class DayCountResource(Resource):
    @namespace.marshal_with(day_count_model, description='Fetches the article count for the date')
    @namespace.response(400, 'The provided date is invalid')
    @cache.cached(timeout=20)
    def get(self, year: int, month: int, day: int):
        try:
            date = datetime.datetime(year, month, day)
        except ValueError:
            raise BadRequest()
        result = DayCountModel.query.filter_by(date=date).first()
        if not result:
            return { 'date': date, 'count': 0 }
        return result


@namespace.route('/top/<int:count>')
class DayCountTopResource(Resource):
    @namespace.marshal_with(day_count_model, as_list=True, 
                            description='Fetches the top article counts (Amount is capped by count but can be less)')
    @cache.cached(timeout=20)
    def get(self, count):
        return DayCountModel.query.order_by(DayCountModel.count.desc()).limit(count).all()
