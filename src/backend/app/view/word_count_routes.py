from flask_restx import Namespace, Resource, fields
from app import api, cache
from app.model.api.pagination import pagination_parser, pagination_model
from app.controller.cache.memcached_backend import cache_api_with_parser
from shared.database.word_count import WordCountModel
from shared.database.top_words import TopWordModel

namespace = Namespace('word-count', description='Routes to retrieve the number of times a word occurred in a headline')
api.add_namespace(namespace)

word_count_model = api.model('WordCount', {
    'word': fields.String,
    'count': fields.Integer
})
word_count_paginated_model = api.model('WordCountPaginated', {
    'pagination': fields.Nested(pagination_model),
    'items': fields.Nested(word_count_model, as_list=True)
})


@namespace.route('/')
class WordCountsResource(Resource):
    @cache_api_with_parser('WordCountsPaginated', pagination_parser)
    @namespace.marshal_with(word_count_paginated_model)
    @namespace.expect(pagination_parser)
    def get(self, args):
        page = 1 if args['page'] is None else args['page']
        page_size = 20 if args['psize'] is None else min(args['psize'], 1024)
        query = WordCountModel.query
        pagination = query.paginate(page=page, per_page=page_size, error_out=True)
        return {
            'pagination': pagination,
            'items': pagination.items
        }


@namespace.route('/<word>')
class WordCountResource(Resource):
    @namespace.marshal_with(word_count_model, description='Fetches the count that this word was in a headline')
    @cache.cached(timeout=20)
    def get(self, word: str):
        result = WordCountModel.query.filter_by(word=word).first()
        if not result:
            return { 'word': word, 'count': 0 }
        return result


@namespace.route('/top/<int:count>')
class WordCountTopResource(Resource):
    @namespace.marshal_with(word_count_model, as_list=True, 
                            description='Fetches the top word counts (Amount is capped by count but can be less)')
    @cache.cached(timeout=20)
    def get(self, count):
        top_words = TopWordModel.query.order_by(TopWordModel.count.desc()) \
                                 .limit(count).all()
        return [top_word for top_word in top_words]
