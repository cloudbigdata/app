from flask_restx import Namespace, Resource, fields, reqparse
from app import api, cache
from app.controller.cache.memcached_backend import cache_api_with_parser
from app.controller.kafka.view_publisher import publish_article_view
from app.model.api.pagination import pagination_model
from shared.database.article import ArticleModel
from shared.database.article_views import ArticleViewsModel
from shared.database.top_articles import TopArticleModel

namespace = Namespace('articles', description='Routes to retrieve articles')
api.add_namespace(namespace)

article_search_parser = reqparse.RequestParser()
article_search_parser.add_argument('q', type=str, help='Text search query', required=False)
article_search_parser.add_argument('page', type=int, help='Page to retrieve (starts at 1)', required=False)
article_search_parser.add_argument('psize', type=int, help='Page size (default: 20, max: 1024)', required=False)

article_views_model = api.model('ArticleViews', {
    'count': fields.Integer,
    'last': fields.DateTime
})
article_model = api.model('Article', {
    'id': fields.Integer,
    'date': fields.Date,
    'headline': fields.String,
    'views': fields.Nested(article_views_model)
})
article_paginated_model = api.model('ArticlesPaginated', {
    'pagination': fields.Nested(pagination_model),
    'items': fields.Nested(article_model, as_list=True)
})


@namespace.route('/')
class ArticlesResource(Resource):
    @cache_api_with_parser('ArticlesPaginated', article_search_parser)
    @namespace.marshal_with(article_paginated_model)
    @namespace.expect(article_search_parser)
    @namespace.response(404, 'Pagination is out of range')
    def get(self, args):
        text_query = args['q']
        page = 1 if args['page'] is None else args['page']
        page_size = 20 if args['psize'] is None else min(args['psize'], 1024)

        query = ArticleModel.query
        if text_query is not None:
            tq = '%' + text_query + '%'
            query = query.filter(
                        ArticleModel.headline.ilike(tq)
                    )
        pagination = query.paginate(page=page, per_page=page_size, error_out=True)
        
        return {
            'pagination': pagination,
            'items': pagination.items
        }

@namespace.route('/<int:id>')
class ArticleResource(Resource):
    @publish_article_view
    @namespace.marshal_with(article_model)
    @namespace.response(404, 'Article not found')
    @cache.cached(timeout=20)
    def get(self, id):
        return ArticleModel.query.filter_by(id=id).first_or_404()


@namespace.route('/top/<int:count>')
class ArticleCountTopResource(Resource):
    @namespace.marshal_with(article_model, as_list=True, 
                            description='Fetches the top viewed articles (Amount is capped by count but can be less)')
    @cache.cached(timeout=20)
    def get(self, count):
        top_articles = TopArticleModel.query.order_by(TopArticleModel.count.desc()) \
                                 .limit(count).all()
        return [top_article.article for top_article in top_articles]
