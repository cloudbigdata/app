from flask_restx import Namespace, Resource, fields
from app import api, cache
from shared.database.sum_article_count import SumArticleCountModel
from shared.database.sum_view_count import SumViewCountModel


namespace = Namespace('total', description='Routes to retrieve total counts')
api.add_namespace(namespace)

count_model = api.model('Count', {
    'count': fields.Integer(attribute='sumCount')
})


@namespace.route('/articles')
class TotalArticlesResource(Resource):
    @namespace.marshal_with(count_model)
    @cache.cached(timeout=20)
    def get(self):
        c = SumArticleCountModel.query.first()
        if not c:
            return { 'sumCount': 0 }
        return c


@namespace.route('/views')
class TotalViewsResource(Resource):
    @namespace.marshal_with(count_model)
    @cache.cached(timeout=20)
    def get(self):
        c = SumViewCountModel.query.first()
        if not c:
            return { 'sumCount': 0 }
        return c
