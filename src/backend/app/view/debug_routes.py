from flask_restx import Namespace, Resource, fields
from app import api, cache

namespace = Namespace('debug', description='Endpoints to retrieve internal state')
api.add_namespace(namespace)


@namespace.route('/cache-servers')
class CacheServersResource(Resource):
    @api.response(200, "Successfully fetched server list", fields.List(fields.String))
    def get(self):
        if not hasattr(cache.cache, '_client'):
            return ["Local cache"]
        return cache.cache._client.addresses
