import socket
from flask import g
from app import app


@app.after_request
def after_request(response):
    cached = g.pop('cached', None)
    response.headers["X-Backend-Server"] = socket.gethostname()
    if cached is not None:
        response.headers["X-Cache"] = "HIT" if cached else "MISS"
    return response
