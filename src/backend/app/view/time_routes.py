import datetime
from flask_restx import Namespace, Resource, fields
from app import api
from shared.database.server_time import ServerTimeModel

namespace = Namespace('server-time', description='Current time of the server')
api.add_namespace(namespace)

time_model = api.model('Time', {
    'current_time': fields.DateTime
})


@namespace.route('/')
class TimeResource(Resource):
    @namespace.marshal_with(time_model)
    def get(self):
        t = ServerTimeModel.query.first()
        if not t:
            return { 'current_time': datetime.datetime.now() }
        return t
