import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restx import Api
from flask_socketio import SocketIO
from flask_caching import Cache
from kafka.producer.kafka import KafkaProducer
from app.controller.utils.prefix_middleware import PrefixMiddleware
from app.controller.utils import database_model_search, route_search, thread_search
from app.controller.cache import memcached_discovery
from shared.configuration import Config

config = Config()
app_config = {
    'DEBUG': False,
    'SECRET_KEY': config.get('Server', 'secret_key'),
    'SQLALCHEMY_DATABASE_URI': config.get('Database', 'uri'),
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    #'CACHE_TYPE': 'app.controller.cache.simple_backend.create',
    'CACHE_TYPE': 'app.controller.cache.memcached_backend.create',
    'CACHE_MEMCACHED_SERVERS': memcached_discovery.get_servers(config.get('Cache', 'hostname')),
    'RESTX_MASK_SWAGGER': False
}
app: Flask = Flask(__name__)
app.config.from_mapping(app_config)
app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=config.get('Server', 'application_root'))
db: SQLAlchemy = SQLAlchemy(app)
api: Api = Api(app)
socketio: SocketIO = SocketIO(app, cors_allowed_origins="*", path='/api/socket.io',
                              logger=False, engineio_logger=False,
                              message_queue='kafka://' + config.get('Kafka', 'bootstrap_server'))
cache: Cache = Cache(app)
kafka_producer: KafkaProducer = KafkaProducer(bootstrap_servers=config.get('Kafka', 'bootstrap_server'))

logging.getLogger('werkzeug').setLevel(logging.ERROR)


def init():
    database_model_search.import_database_models(db)
    route_search.import_routes()
    thread_search.import_threads()
