import threading
import app

def create_app():
    app.init()
    return app.app
    

if __name__ == "__main__":
    app.socketio.run(create_app(), host="0.0.0.0", port=8080)
