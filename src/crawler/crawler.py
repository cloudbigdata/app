import time
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers='my-cluster-kafka-bootstrap:9092')

while True:
    filetosend = open('data/abcnews-date-text_full.csv', "r")

    for line in filetosend:
        while True:
            try:
                future = producer.send("newsline_topic",line.encode())
                result = future.get(timeout=5)
                time.sleep(0.05)
                break
            except Exception:
                print("Kafka not available")
                time.sleep(10)

    filetosend.close()
    