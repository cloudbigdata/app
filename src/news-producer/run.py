from time import sleep
from kafka import KafkaProducer
from shared.configuration import Config

# Get the shared config
config = Config()
bootstrap_server = config.get('Kafka', 'bootstrap_server')
producer = KafkaProducer(bootstrap_servers=bootstrap_server)

# Date after which are published on Kafka
skip_until = "20100101"
skip_finished = False

# Date after which new headlines are delayed
fast_forward_until = "20110311"
fast_forward_finished = False

# Reads headlines from csv 
while True:
    filetosend = open('data/abcnews-date-text_full.csv', "r")
    for line in filetosend:
        if not skip_finished:
            if line.startswith(skip_until):
                skip_finished = True
            else:
                continue
        while True:
            try:
                future = producer.send("headlines", line.encode())
                result = future.get(timeout=5)
                if not fast_forward_finished:
                    sleep(0.05)
                elif line.startswith(fast_forward_until):
                    fast_forward_finished = True
                break
            except Exception as e:
                print(e)
    filetosend.close()
