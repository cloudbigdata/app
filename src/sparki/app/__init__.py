from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from time import sleep
from shared.configuration import Config


# Get the shared Config from config.ini
config = Config()
app_config = {
    'SECRET_KEY': config.get('Server', 'secret_key'),
    'SQLALCHEMY_DATABASE_URI': config.get('Database', 'uri'),
    'SQLALCHEMY_TRACK_MODIFICATIONS': False 
}
app = Flask(__name__)
app.config.from_mapping(app_config)
db = SQLAlchemy(app)


# Check database connection
while True:
    try:
        db.engine.execute("select 1")
        print("Connected to database.")
        break
    except:
        print("Cannot connect to database. Trying again in 5s...")
        sleep(5)
# We use Flask here only to use Flask-SQLAlchemy and not as a server.
# We do this, because Flask-SQLAlchemy is not 100% compatible with sqlalchemy.
# But to still use the same shared database models we use Flask-SQLAlchemy.
