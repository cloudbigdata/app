import json
import pyspark.sql.functions as F
from kafka import KafkaProducer
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import IntegerType, StructType, TimestampType
from app import config
from sqlalchemy.sql import func

# Init Kafka-Producer to publish successful Batch
kafka_producer: KafkaProducer = KafkaProducer(bootstrap_servers=config.get('Kafka', 'bootstrap_server'))

# Common Word -> Filter Word Cloud
common_words = ['the', 'of', 'the', 'of', 'and', 'to', 'a', 'in', 'for', 'is', 'on', 'that', 'by', 'this',
                'with', 'i', 'you', 'it', 'not', 'or', 'be', 'are', 'from', 'at', 'as', 'your', 'all',
                'have', 'new', 'more', 'an', 'was', 'we', 'will', 'home', 'can', 'us', 'about', 'if',
                'still', 'over', 'up', 'after', 'wa', 'sa', 'nt', 'no']

# Processing Time for Batch
processing_time = '25 seconds'

# Create a spark session
spark = SparkSession.builder \
    .appName("Big Data Project").getOrCreate()

# Set log level
spark.sparkContext.setLogLevel('WARN')

# Begin ---  Read messages from Kafka
# Get Headlines from Kafka
headlineMessages = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers",
            config.get('Kafka', 'bootstrap_server')) \
    .option("subscribe", "headlines") \
    .option("startingOffsets", "earliest") \
    .load()

# Get current Views of Articles from WebApp
articleViewMessages = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers",
            config.get('Kafka', 'bootstrap_server')) \
    .option("subscribe", "article_views") \
    .option("startingOffsets", "earliest") \
    .load()

# End --- Read messages from Kafka

# Begin --- Convert the Data into Dataframes
# --- Article Views --- 

articleViewSchema = StructType() \
    .add("id", IntegerType()) \
    .add("timestamp", TimestampType())

articleDf = articleViewMessages.select(
    from_json(
        F.col("value").cast("string"),
        articleViewSchema
    ).alias("json")
).select(
    F.col('json.timestamp').alias("access"),
    F.col("json.*")
) \
    .withColumnRenamed('json.id', 'id')

# --- Headlines --- 

tableColumns = headlineMessages.select(
    F.from_csv(
        F.col("value").cast("string"),
        "publish_date string, headline_text string"
    ).alias("csv")
).select(
    to_date(F.col('csv.publish_date'), 'yyyyMMdd').alias('publish_date'),
    F.col('csv.headline_text')
)

# END --- Convert the Data into Dataframes

# -----------------View Count--------------------

view_count = articleDf \
    .groupBy(F.col('id')) \
    .count() \
    .withColumnRenamed('count', 'views')

# -----------------Last Access--------------------

lastAccessDf = articleDf \
    .groupBy(F.col('id')) \
    .agg(F.max('access').alias('lastAccess'))

# -----------------Date Count--------------------

dateCount = tableColumns \
    .groupBy(F.col('publish_date')) \
    .count() \
    .withColumnRenamed('count', 'dateCount')

articles = tableColumns.select(F.col('publish_date'), F.col('headline_text'))

# -----------------Word Count--------------------

wordCount = tableColumns \
    .withColumn('word', F.explode(F.split(F.col('headline_text'), ' '))) \
    .groupBy('word') \
    .count() \
    .withColumnRenamed('count', 'wordCount')

# -----------------Current Date--------------------
currentDate = tableColumns \
    .select(F.col('publish_date')) \
    .agg(F.max('publish_date').alias('currentDate'))


# Begin --- Database Savings
def save_to_database_article(batchDataframe, batchId):
    """
    Save Headline and Date from Kafka into table article
    Total count of articles and save into table sum_article_count
    """
    def save_to_db(iterator):
        from app import db
        from shared.database.article import ArticleModel
        from shared.database.sum_article_count import SumArticleCountModel
        for row in iterator:
            article = ArticleModel.query.filter_by(headline=row.headline_text).first()
            if (article is None):
                o = ArticleModel(row.publish_date, row.headline_text)
                db.session.add(o)
        # Truncate
        SumArticleCountModel.query.delete()
        # Count
        sumArticles = ArticleModel.query.count()
        # Create viewCountModel
        articleCountModel = SumArticleCountModel(sumArticles)
        db.session.add(articleCountModel)
        db.session.commit()

    batchDataframe.foreachPartition(save_to_db)
    # Publish finish batch
    kafka_producer.send("batch_complete", json.dumps({'id': 'articles'}).encode())


def save_to_database_current_time(batchDataframe, batchId):
    """
    Save current time from Kafka into table server_time
    """

    def save_to_db(iterator):
        from app import db
        from shared.database.server_time import ServerTimeModel

        for row in iterator:
            ServerTimeModel.query.delete()
            o = ServerTimeModel(row.currentDate)
            db.session.merge(o)
        db.session.commit()

    batchDataframe.foreachPartition(save_to_db)
    # Publish finish batch
    kafka_producer.send("batch_complete", json.dumps({'id': 'server_time'}).encode())



def save_to_database_date_count(batchDataframe, batchId):
    """
    Save number of articles per Date
    """
    def save_to_db(iterator):
        from app import db
        from shared.database.day_count import DayCountModel

        for row in iterator:
            o = DayCountModel(row.publish_date, row.dateCount)
            db.session.merge(o)
        db.session.commit()

    batchDataframe.foreachPartition(save_to_db)
    # Publish finish batch
    kafka_producer.send("batch_complete", json.dumps({'id': 'date_count'}).encode())


def save_to_database_word_count(batchDataframe, batchId):
    """
    Save the seperated words from Headline and their count into table word_count
    """
    def save_to_db(iterator):
        from app import db
        from shared.database.word_count import WordCountModel
        from shared.database.top_words import TopWordModel

        for row in iterator:
            if row.word not in common_words:
                o = WordCountModel(row.word, row.wordCount)
                db.session.merge(o)

        # Truncate
        TopWordModel.query.delete()
        # Get Top 100 Articles Views
        counts = WordCountModel.query.order_by(WordCountModel.count.desc()).limit(100).all()
        for i in counts:
            db.session.add(TopWordModel(i))

        db.session.commit()

    batchDataframe.foreachPartition(save_to_db)
    # Publish finish batch
    kafka_producer.send("batch_complete", json.dumps({'id': 'word_count'}).encode())


def update_database_inc_article_view_count(batchDataframe, batchId):

    """
    1. Updates/Creates view count per Article in table article_view
    2. Saves top 100 into table top_articles
    3. Calculate total view of all articles and save into table sum_article_count
    """
    def save_to_db(iterator):
        from app import db
        from shared.database.article_views import ArticleViewsModel
        from shared.database.article import ArticleModel
        from shared.database.sum_view_count import SumViewCountModel
        from shared.database.top_articles import TopArticleModel

        for row in iterator:
            article = ArticleModel.query.filter_by(id=row.id).first()
            articleView = ArticleViewsModel.query.filter_by(article_id=row.id).first()
            if articleView is None:
                o = ArticleViewsModel(row.id, 1)
                db.session.merge(o)
            else:
                article.views.count = article.views.count + 1

        # Truncate
        TopArticleModel.query.delete()
        # Get Top 100 Articles Views
        counts = ArticleModel.query.join(ArticleModel.views) \
            .order_by(ArticleViewsModel.count.desc()).limit(100).all()
        for i in counts:
            db.session.add(TopArticleModel(i))

        # Truncate
        SumViewCountModel.query.delete()
        # Check if table article_view is empty
        if ArticleViewsModel.query.count() != 0:
            # Sum View Count
            sum_view = db.session.query(func.sum(ArticleViewsModel.count))
            # Create viewCountModel
            view_count_model = SumViewCountModel(sum_view)
            db.session.add(view_count_model)
        db.session.commit()

    batchDataframe.foreachPartition(save_to_db)
    # Publish finish batch
    kafka_producer.send("batch_complete", json.dumps({'id': 'article_view_count'}).encode())


def update_database_article_last_access(batchDataframe, batchId):
    """
    Updates/Sets last access of Article in table article_view
    """
    def save_to_db(iterator):
        from app import db
        from shared.database.article_views import ArticleViewsModel
        from shared.database.article import ArticleModel

        for row in iterator:
            article = ArticleModel.query.filter_by(id=row.id).first()
            article_view = ArticleViewsModel.query.filter_by(article_id=row.id).first()
            if article_view is None:
                o = ArticleViewsModel(row.id, 1, row.lastAccess)
                db.session.merge(o)
            else:
                article.views.last = row.lastAccess
        db.session.commit()

    batchDataframe.foreachPartition(save_to_db)
    # Publish finish batch
    kafka_producer.send("batch_complete", json.dumps({'id': 'last_access'}).encode())


# End --- Database Savings

# Begin --- DataStreams

# Date Count Datastreams
dbInsertStreamDateCount = dateCount \
    .writeStream \
    .trigger(processingTime=processing_time) \
    .outputMode("update") \
    .foreachBatch(save_to_database_date_count) \
    .start()

# Word Count Datastreams
dbInsertStreamWordCount = wordCount \
    .writeStream \
    .trigger(processingTime=processing_time) \
    .outputMode("update") \
    .foreachBatch(save_to_database_word_count) \
    .start()

# Current Date Datastreams
dbInsertStreamCurrentDate = currentDate \
    .writeStream \
    .trigger(processingTime=processing_time) \
    .outputMode("update") \
    .foreachBatch(save_to_database_current_time) \
    .start()

#  Article Datastreams
dbInsertStreamArticles = articles \
    .writeStream \
    .trigger(processingTime=processing_time) \
    .outputMode("append") \
    .foreachBatch(save_to_database_article) \
    .start()


#  View count Datastreams
dbStreamIncreaseArticleCount = view_count \
    .writeStream \
    .trigger(processingTime=processing_time) \
    .outputMode("update") \
    .foreachBatch(update_database_inc_article_view_count) \
    .start()

#  Article last access Datastreams
dbStreamLastAccess = lastAccessDf \
    .writeStream \
    .trigger(processingTime=processing_time) \
    .outputMode("update") \
    .foreachBatch(update_database_article_last_access) \
    .start()

# # End --- DataStreams

spark.streams.awaitAnyTermination()
