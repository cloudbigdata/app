FROM bitnami/spark:3-debian-10
LABEL maintainer="Dennis Pfisterer, http://dennis-pfisterer.de"
WORKDIR /app

#------------------------------------
# Set execution environment

# Java Dependencies for Spark Kafka
# These are obtained using Ivy later
ENV SPARK_VERSION "3.0.0"
ENV SPARK_KAFKA_DEPENDENCY "org.apache.spark:spark-sql-kafka-0-10_2.12:${SPARK_VERSION}"
ENV PYSPARK_PYTHON /app/venv/bin/python
ENV PYTHONPATH /app
ENV IVY_PACKAGE_DIR "/tmp/.ivy"

#------------------------------------
# Prepare the system

USER root

# Workaround for "failure to login" error message: 
# cf. https://stackoverflow.com/questions/41864985/hadoop-ioexception-failure-to-login/56083736
RUN groupadd --gid 1001 spark
RUN useradd --uid 1001 --gid spark --shell /bin/bash spark
# End: Workaround

RUN apt-get update && apt-get install -y zip libpq-dev python3 python3-dev python3-psycopg2 gcc
RUN pip3 install virtualenv
RUN chown spark:spark /app/

USER spark

WORKDIR /app

# Create virtualenv
RUN python3 -m venv /app/venv

#------------------------------------
# Prepare dependencies in ZIP file

# Pre-Install Maven dependencies by running an empty python file using spark-submit
# to benefit from Docker's build cache
RUN touch /tmp/empty.py && spark-submit --verbose \
	--conf "spark.jars.ivy=${IVY_PACKAGE_DIR}" \
	--packages "${SPARK_KAFKA_DEPENDENCY}" \
	/tmp/empty.py && rm -f /tmp/empty.py

# Prepare the app's python dependencies
ADD --chown=spark:spark sparki/requirements.txt .
RUN . /app/venv/bin/activate && pip3 install --no-cache-dir -r requirements.txt

#------------------------------------
# Copy the application code into the container

# Copy application code
COPY --chown=spark:spark shared ./shared
RUN zip -r /app/shared shared/*
COPY --chown=spark:spark sparki ./
RUN zip -r /app/app app/*

#------------------------------------
# Set entrypoint and use dependencies zip file

# Use YARN deployment in client mode (requires Hadoop config)
# --master yarn --deploy-mode client"
#	--master k8s://http://192.168.178.42 \
#        --deploy-mode cluster \
# Use YARN deployment in cluster mode (requires Hadoop config)
# --master yarn --deploy-mode cluster

# Use local deployment
# --master local

ENTRYPOINT spark-submit --verbose \
	#--master yarn --deploy-mode client \
	 --master local\
	--conf "spark.jars.ivy=${IVY_PACKAGE_DIR}" \
	--conf "log4j.rootCategory=WARN" \
	--packages "${SPARK_KAFKA_DEPENDENCY}" \
	--py-files app.zip \
	--py-files shared.zip \
	main.py

CMD [""]

