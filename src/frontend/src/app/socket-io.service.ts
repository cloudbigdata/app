import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { io } from 'socket.io-client';


@Injectable({
  providedIn: 'root'
})
export class SocketIoService {

  socket;

  constructor() {
    // creates the socket.io instance and connects to the endpoint
    this.socket = io({
      'path': '/api/socket.io'
    });
    this.socket.on('connect', function () {
      console.log('Connected');
    });
  }

  /**
   * Creates and returns an observable of the socket on a topic.
   * @param topic The Topic on which the socket observable subscribes.
   */
  public subscribe(topic: string) {
    return new Observable<any>((observer: any) => {
      this.socket.on(topic, (message: any) => {
        observer.next(message);
      });
    });
  }

}
