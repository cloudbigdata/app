import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HandleError, ErrorService } from './error.service';
import { catchError } from 'rxjs/internal/operators';


@Injectable({
  providedIn: 'root'
})
export class ServerTimeService {

  private entityUrl = '/api/server-time';
  private readonly handlerError: HandleError;

  constructor(private http: HttpClient, private httpErrorHandler: ErrorService) {
    this.handlerError = httpErrorHandler.createHandleError('ServerTimeService');
  }

  /**
   * Performs an Api get request and returns the backend server time.
   */
  getDateTime(): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/')
      .pipe(
        catchError(this.handlerError('getDateTime', []))
      );
  }
}
