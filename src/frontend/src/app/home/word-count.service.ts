import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { ErrorService, HandleError } from '../error.service';

@Injectable({
  providedIn: 'root'
})
export class WordCountService {

  private entityUrl = '/api/word-count';
  private readonly handlerError: HandleError;

  constructor(private http: HttpClient, private httpErrorHandler: ErrorService) {
    this.handlerError = httpErrorHandler.createHandleError('WordCountService');
  }

  /**
   * Performs an Api get request and returns the number of times a word occurred in a headline.
   */
  getWordCount(): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/')
      .pipe(
        catchError(this.handlerError('getWordCount', []))
      );
  }

  /**
   * Performs an Api get request and returns the top X words of the headings
   * @param count Count how many of the top words should be returned.
   */
  getTopWords(count : number): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/top/' + count)
      .pipe(
        catchError(this.handlerError('getTopWords', []))
      );
  }

  /**
   * Performs an Api get request and returns how often a word appeared in all headings.
   * @param word The word for which the amount is to be returned.
   */
  getCountOfWord (word : string): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/' + word)
      .pipe(
        catchError(this.handlerError('getCountOfWord', []))
      );
  }
}
