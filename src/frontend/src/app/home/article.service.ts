import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HandleError, ErrorService } from '../error.service';
import { catchError } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private entityUrl = '/api/articles';
  private readonly handlerError: HandleError;

  constructor(private http: HttpClient, private httpErrorHandler: ErrorService) {
    this.handlerError = httpErrorHandler.createHandleError('ArticleService');
  }

  /**
   * Performs an Api get request and returns the top X of viewed articles.
   * @param count Count how many of the top articles should be returned.
   */
  getTopViewed(count: number): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/top/' + count)
      .pipe(
        catchError(this.handlerError('getTopViewed', []))
      );
  }
}
