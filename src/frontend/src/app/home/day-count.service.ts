import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HandleError, ErrorService } from '../error.service';
import { catchError } from 'rxjs/internal/operators';

@Injectable()
export class DayCountService {

  private entityUrl = '/api/day-count';
  private readonly handlerError: HandleError;

  constructor(private http: HttpClient, private httpErrorHandler: ErrorService) {
    this.handlerError = httpErrorHandler.createHandleError('DayCountService');
  }

  /**
   * Performs an Api get request and returns the number of articles per day.
   */
  getDayCount(): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/')
      .pipe(
        catchError(this.handlerError('getDayCount', []))
      );
  }

  /**
   * Performs an Api get request and returns the top X number of articles per day.
   * @param count Count how many of the top articles should be returned.
   */
  getTopCounts(count: number): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/top/' + count)
      .pipe(
        catchError(this.handlerError('getTopCounts', []))
      );
  }

  /**
   * Performs an Api-Get request and returns the number of items per day for a given date.
   * @param year Year information of the date.
   * @param month Month information of the date.
   * @param day Day information of the date.
   */
  getTopOfDay(year: number, month: number, day: number): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/' + year + '/' + month + '/' + day)
      .pipe(
        catchError(this.handlerError('getTopOfDay', []))
      );
  }
}
