import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { ErrorService, HandleError } from '../error.service';

@Injectable({
  providedIn: 'root'
})
export class TotalService {

  private entityUrl = '/api/total';
  private readonly handlerError: HandleError;

  constructor(private http: HttpClient, private httpErrorHandler: ErrorService) {
    this.handlerError = httpErrorHandler.createHandleError('TotalService');
  }

  /**
   * Performs an Api get request and returns the total number of articles in the system.
   */
  getTotalArticles(): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/articles')
      .pipe(
        catchError(this.handlerError('getTotalArticles', []))
      );
  }

  /**
   * Performs an Api get request and returns the total count of views.
   */
  getTotalViews(): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/views')
      .pipe(
        catchError(this.handlerError('getTotalViews', []))
      );
  }
}
