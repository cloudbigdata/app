import { TestBed } from '@angular/core/testing';

import { DayCountService } from './day-count.service';

describe('DayCountService', () => {
  let service: DayCountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DayCountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
