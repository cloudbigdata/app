import { Component, Input, OnInit } from '@angular/core';
import { formatDate } from '@angular/common';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts/ng2-charts';

@Component({
  selector: 'app-day-count-diagram',
  templateUrl: './day-count-diagram.component.html',
  styleUrls: ['./day-count-diagram.component.css']
})
export class DayCountDiagramComponent implements OnInit {
  @Input() lineChartData: ChartDataSets[] = [];
  @Input() lineChartLabels: Label[] = [];
  public lineChartColors: Color[] = [
    {
      borderColor: 'white',
    },
  ];
  public lineChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          id: 'x-axis',
          type: 'time',
          distribution: 'linear',
          position: 'bottom',
          gridLines: {
            color: 'rgba(255,255,255,0.3)',
          },
          ticks: {
            fontColor: 'white',
          },
          time: {
            unit: 'day',
            displayFormats: {
              day: 'DD.MM.yyyy'
            }
          }
        }
      ],
      yAxes: [
        {
          id: 'y-axis',
          position: 'left',
          gridLines: {
            color: 'rgba(255,255,255,0.3)',
          },
          ticks: {
            fontColor: 'white',
          }
        }
      ]
    },
    tooltips: {
      callbacks: {
        title: function(tooltipItem, data) {
          let date = new Date(String(tooltipItem[0].label));
          return  formatDate(date, 'dd.MM.yyyy', 'en-US');
        }
      }
    },
    legend: {
      display: false
    }
  };
  public lineChartLegend = true;
  public lineChartType : ChartType = 'line';
  public lineChartPlugins = [];

  constructor() { }

  ngOnInit(): void { }

}
