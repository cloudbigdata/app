import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayCountDiagramComponent } from './day-count-diagram.component';

describe('DayCountDiagramComponent', () => {
  let component: DayCountDiagramComponent;
  let fixture: ComponentFixture<DayCountDiagramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DayCountDiagramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DayCountDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
