import { Component, Input, OnInit, Output } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-count-table-card',
  templateUrl: './count-table-card.component.html',
  styleUrls: ['./count-table-card.component.css']
})
export class CountTableCardComponent implements OnInit {
  @Input() results = new MatTableDataSource<any>().data;
  @Input() columns: any = { displayedColumns: [] };

  constructor() { }

  ngOnInit(): void {
  }

}
