import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountTableCardComponent } from './count-table-card.component';

describe('CountTableCardComponent', () => {
  let component: CountTableCardComponent;
  let fixture: ComponentFixture<CountTableCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountTableCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountTableCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
