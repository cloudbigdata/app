import { Component, OnInit } from '@angular/core';
import { CloudData, CloudOptions } from 'angular-tag-cloud-module';
import { SocketIoService } from 'src/app/socket-io.service';
import { WordCountService } from '../word-count.service';

@Component({
  selector: 'app-word-cloud',
  templateUrl: './word-cloud.component.html',
  styleUrls: ['./word-cloud.component.css'],
})
export class WordCloudComponent implements OnInit {

  errorMessage: any;

  data: CloudData[] = [];
  ready = false;

  constructor(private wordCountService: WordCountService, private socket: SocketIoService) { }

  ngOnInit(): void {
    // subscribe to socket-io on batch-complete
    this.socket.subscribe("batch_complete").subscribe(async (next: any) => {
      let id = JSON.parse(next).id;
      
      if (id === 'word_count') {
        this.updateWordCount();
      }
    });

    // update word-cloud-data
    this.updateWordCount();
  }

  /**
   * async request data for word-cloud from api-request
   */
  async updateWordCount() {

    this.ready = false;
    const results = await this.wordCountService.getTopWords(100).toPromise().catch(error => this.errorMessage = error as any);

    let newData: CloudData[] = [];

    // calculate max word count
    var max = Math.max.apply(Math, results.map(function (o: any) { return o.count; }));

    for (let result of results) {
      var color: number = 0;

      // for the first 10% of data the color fades out faster than the rest of the data, so more values are displayed and not completly faded out
      if (result.count > max * 0.9) {
        // color-range: 66(background-color) - 255, word-count-range: 0 - max, maping word-count- to color-range
        color = (255 - 66) / max * result.count + 66;
      }
      else {
        // color-range: 66(background-color) - 255, word-count-range: 0 - 90% of max, maping word-count- to color-range
        color = (255 - 66) / max * 0.9 * result.count + 66;
      }

      newData.push({ text: result.word, weight: result.count, color: `rgb(${color}, ${color}, ${color})` });
    }
    this.data = newData;
    this.ready = true;
  }
}
