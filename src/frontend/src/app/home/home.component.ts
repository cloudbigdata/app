import { Component, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { SocketIoService } from '../socket-io.service';
import { ArticleService } from './article.service';
import { DayCountService } from './day-count.service';
import { TotalService } from './total.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  errorMessage: any;
  dayCountResultsDate: any[] = [];
  dayCountResultsCount: any[] = [];
  dayCountResultsReady = false;
  dayCountData: ChartDataSets[] = [];
  topCountResults: any[] = [];
  topCountResultsReady = false;
  topCountColumns = {
    displayedColumns: ["date", "count"],
    date: {
      name: 'Date',
      type: 'date'
    },
    count: {
      name: 'Count',
      type: 'string'
    }
  };
  topArticlesResults: any[] = [];
  topArticlesResultsReady = false;
  topArticlesColumns = {
    displayedColumns: ["headline", "views"],
    headline: {
      name: 'Article',
      type: 'string'
    },
    views: {
      name: 'Views',
      type: 'string'
    }
  };
  totalArticlesResult: number = 0;
  totalArticlesResultReady = false;
  totalArticlesChange: number = 0;
  totalViewsResult: number = 0;
  totalViewsResultReady = false;
  totalViewsChange: number = 0;
  topCount: number = 10;

  constructor(private dayCountService: DayCountService,
    private articleService: ArticleService,
    private totalService: TotalService,
    private socket: SocketIoService) { }

  ngOnInit(): void {
    // subscribe to socket-io on batch-complete
    this.socket.subscribe("batch_complete").subscribe(async (next: any) => {
      let id = JSON.parse(next).id;

      switch (id) {
        case "article_view_count":
          // top viewed articles
          this.updateTopViewed();
          // total views
          this.updateTotalViews();
          break;
        case "articles":
          // total articles
          this.updateTotalArticles();
          break;
        case "date_count":
          // Articles per day
          this.updateDayCount();
          // top articles/day
          this.updateTopCounts();
          // articles on day -> at component itself
          break;
        case "word_count":
          // Top Words -> at component itself
          break;
        default:
          break;
      }
    });

    // update all data
    this.updateDayCount();
    this.updateTopCounts();
    this.updateTopViewed();
    this.updateTotalViews();
    this.updateTotalArticles();
  }

  /**
   * get day count with async api-request
   */
  async updateDayCount() {
    this.dayCountResultsReady = false;

    // get result for all articles per day
    const results = await this.dayCountService.getDayCount().toPromise().then(res => res.items).catch(error => {
      this.errorMessage = error as any
    });

    for (let i = 0; i < results.length; i++) {
      let date = new Date(results[i].date);
      results[i].date = date;
    }

    this.dayCountResultsCount = [];
    this.dayCountResultsDate = [];

    for (let i = 0; i < results.length; i++) {
      const element = results[i];
      this.dayCountResultsCount.push(element.count);
      this.dayCountResultsDate.push(element.date);
    }
    this.dayCountData = [{ data: this.dayCountResultsCount, label: 'Published headlines' }];
    this.dayCountResultsReady = true;
  }

  /**
   * get top count with async api-request
   */
  async updateTopCounts() {
    this.topCountResultsReady = false;
    
    // get result for top article counts
    let result = await this.dayCountService.getTopCounts(this.topCount).toPromise().catch(error => {
      this.errorMessage = error as any
    });

    console.log("updateTopCounts", result);
    

    for (let i = 0; i < result.length; i++) {
      let date = new Date(result[i].date);
      result[i].date = date;
    }

    this.topCountResults = result;
    this.topCountResultsReady = true;
  }

  /**
   * get top viewed headlines with async api-request
   */
  async updateTopViewed() {
    this.topArticlesResultsReady = false;
    
    // get result for top articles
    const result = await this.articleService.getTopViewed(this.topCount).toPromise().catch(error => {
      this.errorMessage = error as any
    });

    let r: any = []
    for (let i = 0; i < result.length; i++) {
      r.push({ 'headline': result[i].headline, views: result[i].views.count });
    }

    this.topArticlesResults = r;
    this.topArticlesResultsReady = true;
  }

  /** 
   * get total view-count for all headlines with async api-request
   */
  async updateTotalViews() {
    this.totalViewsResultReady = false;

    // get total views
    const result = await this.totalService.getTotalViews().toPromise().catch(error => {
      this.errorMessage = error as any
    });

    if (this.totalViewsResult != 0) {
      this.totalViewsChange = result.count - this.totalViewsResult;
    }

    this.totalViewsResult = result.count;
    this.totalViewsResultReady = true;
  }

/**
 * get total article-count with async api-request
 */
  async updateTotalArticles() {
    this.totalArticlesResultReady = false;
    
    // get total articles
    const result = await this.totalService.getTotalArticles().toPromise().catch(error => {
      this.errorMessage = error as any
    });

    if (this.totalArticlesResult != 0) {
      this.totalArticlesChange = result.count - this.totalArticlesResult;
    }
    this.totalArticlesResult = result.count;
    this.totalArticlesResultReady = true;
  }

}
