import { NgModule } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { DayCountService } from "./day-count.service";
import { WordCountService } from "./word-count.service";
import { HomeComponent } from "./home.component";
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CountTableCardComponent } from './count-table-card/count-table-card.component';
import { CardCountAtDayComponent } from './card-count-at-day/card-count-at-day.component';
import { WordCloudComponent } from './word-cloud/word-cloud.component';
import { DayCountDiagramComponent } from './day-count-diagram/day-count-diagram.component';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { CUSTOM_DATE_FORMATS } from '../custom-date-formats';
import { MomentDateAdapter } from "@angular/material-moment-adapter";
import { ChartsModule } from 'ng2-charts';
import { TotalService } from "./total.service";
import { CardBigNumberComponent } from './card-big-number/card-big-number.component';



@NgModule({
    imports: [
        MatCardModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatDatepickerModule,
        MatGridListModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatDividerModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        TagCloudModule,
        ChartsModule,
        BrowserModule
    ],
    declarations: [
      HomeComponent,
      CardCountAtDayComponent,
      CountTableCardComponent,
      WordCloudComponent,
      DayCountDiagramComponent,
      CardBigNumberComponent
    ],
    exports: [
      HomeComponent,
      MatCardModule,
      MatFormFieldModule,
      MatNativeDateModule,
      MatInputModule,
      MatDatepickerModule,
      MatGridListModule,
      MatProgressSpinnerModule,
      MatProgressBarModule,
      MatDividerModule,
      FormsModule,
      ReactiveFormsModule,
      MatTableModule
    ],
    providers: [
      DayCountService,
      WordCountService,
      TotalService,
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS }
    ]
})
export class HomeModule {
}