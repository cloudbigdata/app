import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardCountAtDayComponent } from './card-count-at-day.component';

describe('CardCountAtDayComponent', () => {
  let component: CardCountAtDayComponent;
  let fixture: ComponentFixture<CardCountAtDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardCountAtDayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardCountAtDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
