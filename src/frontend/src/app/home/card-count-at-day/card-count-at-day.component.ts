import { Component, Input, OnInit } from '@angular/core';
import { DayCountService } from '../day-count.service';
import {FormControl} from '@angular/forms';
import { SocketIoService } from 'src/app/socket-io.service';

@Component({
  selector: 'app-card-count-at-day',
  templateUrl: './card-count-at-day.component.html',
  styleUrls: ['./card-count-at-day.component.css']
})
export class CardCountAtDayComponent implements OnInit {

  date: Date = new Date();
  count = 0;
  ready = false;
  errorMessage: any;

  selectedDate = new FormControl(new Date());

  constructor(private dayCountService: DayCountService, private socket: SocketIoService) { }

  ngOnInit(): void {
    // subscribe to socket-io on batch-complete
    this.socket.subscribe("batch_complete").subscribe(async (next: any) => {
      let id = JSON.parse(next).id;
      if (id === 'date_count') {
        this.update();
      }
    });

    // update all data async
    this.update();
  }

  /**
   * Sets the new date value from date-picker-input to selected date and updates data
   * @param event 
   */
  inputEvent(event: any){
    this.selectedDate.setValue(event.value._d)
    this.update();
  }

  /**
   * updates all data with an async rest-api-request
   */
  async update(){
    this.ready = false;
    const date = this.selectedDate.value;
    const result = await this.dayCountService.getTopOfDay(date.getFullYear(), date.getMonth() + 1, date.getDate()).toPromise().catch(error => this.errorMessage = error as any);
    this.date = new Date(result.date);
    this.count = result.count
    this.ready = true;
  }
}
