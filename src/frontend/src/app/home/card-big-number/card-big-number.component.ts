import { Component, Input, OnInit } from '@angular/core';
import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-card-big-number',
  templateUrl: './card-big-number.component.html',
  styleUrls: ['./card-big-number.component.css'],
  animations: [
    trigger('fadeAnimation', [
      state('in', style({opacity: 0})),
      transition(':enter', [
        animate('10s', keyframes([
          style({opacity: 1}),
          style({opacity: 0}),
        ]))
      ])
    ])
  ]
})
export class CardBigNumberComponent implements OnInit {

  @Input() value: number = 0;
  @Input() change: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
