import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBigNumberComponent } from './card-big-number.component';

describe('CardBigNumberComponent', () => {
  let component: CardBigNumberComponent;
  let fixture: ComponentFixture<CardBigNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardBigNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBigNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
