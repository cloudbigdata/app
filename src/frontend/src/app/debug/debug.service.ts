import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { ErrorService, HandleError } from '../error.service';

@Injectable({
  providedIn: 'root'
})
export class DebugService {

  private entityUrl = '/api/debug';

  private readonly handlerError: HandleError;

  constructor(private http: HttpClient, private httpErrorHandler: ErrorService) {
    this.handlerError = httpErrorHandler.createHandleError('DebugService');
  }

  /**
   * request all active cache-server from rest-api
   */
  getCacheServer(): Observable<any> {
    return this.http.get<any>(this.entityUrl + '/cache-servers')
      .pipe(
        catchError(this.handlerError('getCacheServer', []))
      );
  }

}
