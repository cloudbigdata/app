import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-console',
  templateUrl: './card-console.component.html',
  styleUrls: ['./card-console.component.css']
})
export class CardConsoleComponent implements OnInit {

  /**
   * pushes a value to a value array and removes the first value if array-length is greater than 10
   */
  @Input() set value(value: string) {
    if(value) {
      this.values.push({
        date: new Date(),
        text: value
      });
      if(this.values.length > 10) {
        this.values.shift();
      }
    }
  }

  values : any[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
