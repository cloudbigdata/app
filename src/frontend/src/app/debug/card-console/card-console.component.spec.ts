import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardConsoleComponent } from './card-console.component';

describe('CardConsoleComponent', () => {
  let component: CardConsoleComponent;
  let fixture: ComponentFixture<CardConsoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardConsoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardConsoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
