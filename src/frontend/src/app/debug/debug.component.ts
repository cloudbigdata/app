import { Component, OnInit } from '@angular/core';
import { SocketIoService } from '../socket-io.service';

@Component({
  selector: 'app-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.css']
})
export class DebugComponent implements OnInit {

  headlines: string = "";
  updates: string = "";

  constructor(private socketService: SocketIoService) { }

  ngOnInit(): void {
    // subscribe to socket-io on headlines
    this.socketService.subscribe('headlines').subscribe(
      results => {
        this.headlines = String(results);
      }
    )
    // subscribe to socket-io on batch-complete
    this.socketService.subscribe('batch_complete').subscribe(
      results => {
        this.updates = String(JSON.parse(results).id);
      }
    )
  }

}
