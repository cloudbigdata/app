import { Component, OnInit } from '@angular/core';
import { DebugService } from '../debug.service';

@Component({
  selector: 'app-cache-list',
  templateUrl: './cache-list.component.html',
  styleUrls: ['./cache-list.component.css']
})
export class CacheListComponent implements OnInit {

  errorMessage: any;
  interval: any;
  cacheServer : any[] = [];

  constructor(private debugService : DebugService) { }

  ngOnInit(): void {
    // get new data for cache-server
    this.refreshData();
    // create time interval of 5 seconds to refresh the data
    this.interval = setInterval(() => { 
      this.refreshData(); 
  }, 5000);
  }

  ngOnDestroy() {
    // remove the refresh-interval
    clearInterval(this.interval);
}

/**
 * request cache-server-data from rest-api
 */
  refreshData() {
    this.debugService.getCacheServer().subscribe(
      results => {
        this.cacheServer = results;
      },
      error => this.errorMessage = error as any
    );
  }

}
