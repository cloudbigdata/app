import { Component } from '@angular/core';
import { ServerTimeService } from './server-time.service';
import { SocketIoService } from './socket-io.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  serverTime: Date = new Date();
  serverTimeReady = false;

  constructor(private serverTimeService: ServerTimeService,
              private socketService: SocketIoService) {}

  ngOnInit(): void {
    // update servertime once
    this.updateServerTime();

    // subscribe to socket.io on batch_complete
    this.socketService.subscribe('batch_complete').subscribe(
      results => {
        this.updateServerTime();
      }
    );
  }

  /**
   * Updates the server time
   */
  updateServerTime() {
    this.serverTimeReady = false;
    this.serverTimeService.getDateTime().subscribe(
      result => { 
        // update time values
        let date: Date = new Date(result.current_time)
        this.serverTime = date;
        this.serverTimeReady = true;
      }
    )
  }
}
