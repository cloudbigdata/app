import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { ErrorService, HandleError } from '../error.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private entityUrl = '/api/articles';

  private readonly handlerError: HandleError;

  constructor(private http: HttpClient, private httpErrorHandler: ErrorService) {
    this.handlerError = httpErrorHandler.createHandleError('ArticleService');
  }

  /**
   * get all articles on a specific page with optional search-query
   * @param query search-query
   * @param page index of requested page
   * @param pageSize page size
   */
  getArticles(query:string, page: number, pageSize: number): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', `${page+1}`);
    params = params.append('psize', `${pageSize}`);

    if (query.length > 0){
      params = params.append('q', `${query}`);
    }

    return this.http.get<any>(this.entityUrl + '/', {params: params})
      .pipe(
        catchError(this.handlerError('getArticles', []))
      );
  }
 /**
  * get an article of a specific ID
  * @param id 
  */
  getArticle(id: number) : Observable<any> {
    return this.http.get<any>(this.entityUrl + '/' + id, {observe: 'response' as 'body'})
      .pipe(
        catchError(this.handlerError('getArticle', []))
      );
  }
}
