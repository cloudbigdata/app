import { ViewportScroller } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'date', 'headline', 'viewCount', 'viewLast'];
  data: any[] = [];
  dataSource: MatTableDataSource<any>;
  pagination: any;
  ready = false;

  searchForm: FormGroup;

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;

  constructor(private articleService: ArticleService, private formBuilder: FormBuilder, private router: Router) {
    this.dataSource = new MatTableDataSource(this.data);
    this.searchForm = this.formBuilder.group(
      { search: [null], }
    );
  }

  ngOnInit(): void {
    // get first 10 articles to display on table
    this.getNextData(0, 10);
  }

  /**
   * send http-request to get paginated articles
   * @param page page index
   * @param psize page size
   * @param q search-query
   */
  getNextData(page: number, psize: number, q: string = "") {
    this.ready = false;
    this.articleService.getArticles(q, page, psize).subscribe(response => {
      this.pagination = response.pagination;
      this.data = response.items;
      
      this.dataSource = new MatTableDataSource(this.data);
      this.dataSource._updateChangeSubscription();

      this.ready = true
    },
    error => {
      this.ready = true;
    });
  }

  /**
   * when table page changes, load next data page
   * @param event event from paginator-buttons
   */
  pageChanged(event: any) {
    this.dataSource.data = [];
    let nextIndex = event.pageIndex;
    let pageSize = event.pageSize;
    this.getNextData(nextIndex, pageSize);
  }

  /**
   * load articles which match the search query
   */
  search() {
    console.log("on search");
    this.getNextData(0, 10, this.searchForm.value.search.toString())
  }

  /**
   * remove search query
   */
  clearSearch() {
    console.log("on clearSearch");
    this.searchForm.reset();
    this.getNextData(0, 10)
  }

  /**
   * start search with enter-key
   * @param value serach-query
   */
  onEnter(value: string){
    console.log("on enter: ", value);
    
    this.getNextData(0, 10, value)
  }

  /**
   * navigate to selected article
   * @param articleId ID of selected article
   */
  navigateArticle(articleId: any){
    this.router.navigate(['/article/' + articleId]);
  }
  
}
