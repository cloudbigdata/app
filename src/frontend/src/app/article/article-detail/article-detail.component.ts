import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '../article.service';
import { LoremIpsum } from "lorem-ipsum";

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  errorMessage: any;
  result : any;
  backendserver : any;
  cacheresult : any = "MISS";
  viewcount : any = "ViewCount";
  lastview : any = "LastDate";
  content : any;
  ready = false;

  constructor(private route: ActivatedRoute, private articleService: ArticleService) { }

  ngOnInit(): void {
    // get ID of article from route-parameters
    const articleId = this.route.snapshot.params.id;

    // get a specific article from http-service
    this.articleService.getArticle(articleId).subscribe(
      data =>
      {
        this.result = data.body;
        this.backendserver = data.headers.get('x-backend-server');
        this.cacheresult = data.headers.get('x-cache');
        this.ready = true;
      },
      error => this.errorMessage = error as any
    );

    this.content = this.getContent();
  }

  /**
   * generate lorem-ipsum text to fill article
   */
  getContent(): String {
    let text = "";
    for(let i = 0; i < 5; i++) {
      text += "<p>" + loremIpsum.generateParagraphs(3) + "</p>";
    }
    return text;
  }
}

/**
 * configure the lorem-ipsum-generator
 */
const loremIpsum = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});
