import { NgModule } from '@angular/core';
import { AppRoutingModule } from './../app-routing.module';
import { ArticleListComponent } from './article-list/article-list.component';
import { ArticleService } from './article.service';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { LoremIpsum } from 'lorem-ipsum';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ArticleListComponent, ArticleDetailComponent],
  imports: [
    BrowserModule,
    CommonModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatGridListModule,
    MatSortModule,
    MatButtonModule,
    AppRoutingModule,
    MatProgressBarModule,
    MatDividerModule,
    MatSortModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule
  ],
  exports: [
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatGridListModule,
    MatDividerModule,
    MatSortModule,
    MatButtonModule,
    MatProgressBarModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
  ],
  providers: [
    ArticleService,
    LoremIpsum
  ]
})
export class ArticleModule { }
