import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ErrorService} from './error.service';
import { HomeModule } from './home/home.module';
import { ArticleModule } from './article/article.module';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import {FormsModule} from '@angular/forms';
import { CUSTOM_DATE_FORMATS } from './custom-date-formats';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { DebugComponent } from './debug/debug.component';
import { CacheListComponent } from './debug/cache-list/cache-list.component';
import { CardConsoleComponent } from './debug/card-console/card-console.component';



@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    DebugComponent,
    CacheListComponent,
    CardConsoleComponent
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    HomeModule,
    ArticleModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    MatListModule,
    FormsModule,
    MatMomentDateModule,
    MatListModule,
    BrowserAnimationsModule,
    BrowserModule
  ],
  exports: [
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatListModule,
    MatSortModule,
    MatListModule
  ],
  providers: [
    ErrorService,
    { provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
