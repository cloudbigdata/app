from flask_socketio import SocketIO
from shared.configuration import Config
from flask import Flask

config = Config()
bootstrap_server = config.get('Kafka', 'bootstrap_server')

config = Config()
app_config = {
    'DEBUG': False,
    'SECRET_KEY': config.get('Server', 'secret_key'),
}
app: Flask = Flask(__name__)
app.config.from_mapping(app_config)
socketio: SocketIO = SocketIO(app, logger=False, engineio_logger=False,
                              message_queue='kafka://' + config.get('Kafka', 'bootstrap_server'))