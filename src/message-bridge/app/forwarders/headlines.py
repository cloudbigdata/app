from time import time
from kafka import KafkaConsumer
from app import config, socketio


def forward_headlines(bootstrap_server):
    consumer = KafkaConsumer('headlines', bootstrap_servers=bootstrap_server)
    last = 0
    for msg in consumer:
        # We apply a little bit of throttling to avoid oversaturation of the
        # websocket connection.
        if time() - last > 0.1:
            value = msg.value.decode('utf-8').replace("\n", "")
            socketio.emit('headlines', value)
            last = time()
