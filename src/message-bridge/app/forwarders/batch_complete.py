from kafka import KafkaConsumer
from app import config, socketio


def forward_batch_complete(bootstrap_server):
    consumer = KafkaConsumer('batch_complete', bootstrap_servers=bootstrap_server)
    for msg in consumer:
        value = msg.value.decode('utf-8').replace("\n", "")
        socketio.emit('batch_complete', value)
