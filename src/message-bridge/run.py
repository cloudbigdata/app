from threading import Thread
from app import app, socketio, config
from app.forwarders.headlines import forward_headlines
from app.forwarders.batch_complete import forward_batch_complete


headline_thread = Thread(target=forward_headlines, args=(config.get('Kafka', 'bootstrap_server'),))
headline_thread.start()
batch_complete_thread = Thread(target=forward_batch_complete, args=(config.get('Kafka', 'bootstrap_server'),))
batch_complete_thread.start()

# Start a dummy socket to keep application alive.
# No actual request will be processed, because this service
# has no outside connections except kafka.
socketio.run(app)
