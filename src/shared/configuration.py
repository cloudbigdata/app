import os
import logging
from distutils.util import strtobool
import configparser

LOG_LEVELS = {
    'DEBUG': logging.DEBUG,
    'INFO': logging.INFO,
    'WARN': logging.WARN,
    'WARNING': logging.WARN,
    'ERROR': logging.ERROR
}


class Config:
    def __init__(self):
        self.config = configparser.ConfigParser()
        config_files = ['shared/config.ini']
        self.config.read(config_files)

    def get_log_level(self, section, option):
        raw = self.get(section, option)
        level = LOG_LEVELS.get(raw, None)
        if level is None:
            print(raw + " is not a valid logging level for %s, DEBUG is used instead" % option)
            return logging.DEBUG
        return level

    def get_int(self, section, option):
        return int(self.get(section, option))

    def get_float(self, section, option):
        return float(self.get(section, option))

    def get_boolean(self, section, option):
        return bool(strtobool(self.get(section, option)))

    def get(self, section, option):
        return self.config.get(section, option)
