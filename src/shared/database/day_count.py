from app import db

class DayCountModel(db.Model):
    """
    Database Model for total count per date
    """

    __tablename__ = 'day_count'

    date = db.Column(db.Date, primary_key=True)
    count = db.Column(db.Integer)

    def __init__(self, date, count: int):
        self.date = date
        self.count = count

    def __repr__(self):
        return f"<DayCount {self.date}: {self.count}>"
