from app import db

class SumArticleCountModel(db.Model):
    """
    Database Model for total article count
    """
    __tablename__ = 'sum_article_count'

    sumCount = db.Column(db.Integer, primary_key=True)

    def __init__(self, sumCount: int):
        self.sumCount = sumCount

    def __repr__(self):
        return f"<Sum Article_Count: {self.sumCount}>"
