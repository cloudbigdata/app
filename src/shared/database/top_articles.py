from sqlalchemy.orm import relationship
from app import db
from .article import ArticleModel

class TopArticleModel(db.Model):
    """
    Database Model for top 100 articles based on view count
    """
    __tablename__ = 'top_articles'

    article_id = db.Column(db.Integer,db.ForeignKey('article.id'), primary_key=True)
    article = db.relationship("ArticleModel")
    count = db.Column(db.Integer)

    def __init__(self, article):
        self.article = article
        self.count = article.views.count

    def __repr__(self):
        return f"<Article Id: {self.article_id} , Count: {self.count} >"
