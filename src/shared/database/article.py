from app import db
from .article_views import ArticleViewsModel

class ArticleModel(db.Model):
    """
    Database Model for Articles
    Relation: Article <-> Article View
    Article -> Date, Headlines, Views
    """
    __tablename__ = 'article'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False)
    headline = db.Column(db.String, nullable=False, unique=True)
    views = db.relationship('ArticleViewsModel', uselist=False, lazy='joined')

    def __init__(self, date, headline: str):
        self.headline = headline
        self.date = date
        self.views = ArticleViewsModel()

    def __repr__(self):
        return f'<Article {self.id}: {self.headline}>'
