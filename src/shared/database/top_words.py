from app import db
from .word_count import WordCountModel

class TopWordModel(db.Model):
    """
    Database Model for top 100 words based on their apperance
    """
    __tablename__ = 'top_words'

    word = db.Column(db.String, primary_key=True)
    count = db.Column(db.Integer)

    def __init__(self, wordcount):
        self.word = wordcount.word
        self.count = wordcount.count

    def __repr__(self):
        return f"<Word: {self.word} , Count: {self.count} >"
