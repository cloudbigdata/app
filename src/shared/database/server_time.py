from app import db

class ServerTimeModel(db.Model):
    """
    Database Model for the current server time
    """
    __tablename__ = 'server_time'

    current_time = db.Column(db.Date, primary_key=True)

    def __init__(self, current_time):
        self.current_time = current_time

    def __repr__(self):
        return f"<ServerTime {self.current_time}>"
