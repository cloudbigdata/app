from app import db

class SumViewCountModel(db.Model):
    """
    Database Model for total view count
    """
    __tablename__ = 'sum_view_count'

    sumCount = db.Column(db.Integer, primary_key=True)

    def __init__(self, sumCount: int):
        self.sumCount = sumCount

    def __repr__(self):
        return f"<Sum View_Count: {self.sumCount}>"
