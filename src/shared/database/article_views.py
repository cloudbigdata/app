from sqlalchemy.orm import relationship
from app import db

class ArticleViewsModel(db.Model):
    """
    Database Model for Articles Views
    Relation: Article <-> Article View
    Article View -> Article_id, LastAccess and Access Count
    """
    __tablename__ = 'article_views'

    article_id = db.Column(db.Integer, db.ForeignKey('article.id'), primary_key=True)
    article = db.relationship("ArticleModel", back_populates="views") 
    count = db.Column(db.Integer, nullable=False)
    last = db.Column(db.DateTime)

    def __init__(self, article_id: int=None, count: int=0, last=None):
        self.count = count
        if article_id is not None:
            self.article_id = article_id
        self.last = last

    def __repr__(self):
        return f'<ArticleViews {self.article_id}: {self.count}>'
