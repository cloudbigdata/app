from app import db

class WordCountModel(db.Model):
    """
    Database Model for count per word
    """
    __tablename__ = 'word_count'

    word = db.Column(db.String, primary_key=True)
    count = db.Column(db.Integer)

    def __init__(self, word, count: int):
        self.word = word
        self.count = count

    def __repr__(self):
        return f"<WordCount {self.word}: {self.count}>"
